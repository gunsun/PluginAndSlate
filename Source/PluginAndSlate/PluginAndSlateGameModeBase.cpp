// Copyright Epic Games, Inc. All Rights Reserved.


#include "PluginAndSlateGameModeBase.h"
#include "AnotherGameInvoker.h"

void APluginAndSlateGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	UAnotherGameInvoker* Lib = NewObject<UAnotherGameInvoker>();
	Lib->InvokeLib();
}
