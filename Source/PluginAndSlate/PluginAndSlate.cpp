// Copyright Epic Games, Inc. All Rights Reserved.

#include "PluginAndSlate.h"

void FPluginAndSlateModule::StartupModule()
{
}

void FPluginAndSlateModule::ShutdownModule()
{
}

IMPLEMENT_PRIMARY_GAME_MODULE(FPluginAndSlateModule, PluginAndSlate, "PluginAndSlate");