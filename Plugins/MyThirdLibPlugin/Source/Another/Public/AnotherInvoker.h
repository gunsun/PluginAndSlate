// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "UObject/NoExportTypes.h"
#include "AnotherInvoker.generated.h"

/**
 * 
 */
UCLASS()
class ANOTHER_API UAnotherInvoker : public UObject
{
	GENERATED_BODY()
public:
	void InvokeLib();
};
