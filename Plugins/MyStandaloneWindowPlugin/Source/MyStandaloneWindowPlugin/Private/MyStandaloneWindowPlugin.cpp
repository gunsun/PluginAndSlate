// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyStandaloneWindowPlugin.h"
#include "MyStandaloneWindowPluginStyle.h"
#include "MyStandaloneWindowPluginCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Input/SSlider.h"
#include "Widgets/SBoxPanel.h"
#include "ToolMenus.h"
#include "SMyWidget.h"

static const FName MyStandaloneWindowPluginTabName("MyStandaloneWindowPlugin");
static const FName MyTab0Name("MyTab0");
static const FName MyTab1Name("MyTab1");
static const FName SMyWidgetName("SMyWidget");

#define LOCTEXT_NAMESPACE "FMyStandaloneWindowPluginModule"

void FMyStandaloneWindowPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FMyStandaloneWindowPluginStyle::Initialize();
	FMyStandaloneWindowPluginStyle::ReloadTextures();

	FMyStandaloneWindowPluginCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FMyStandaloneWindowPluginCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FMyStandaloneWindowPluginModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FMyStandaloneWindowPluginModule::RegisterMenus));
	
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MyStandaloneWindowPluginTabName,
		FOnSpawnTab::CreateRaw(this, &FMyStandaloneWindowPluginModule::OnSpawnPluginTab))
			.SetDisplayName(LOCTEXT("FMyStandaloneWindowPluginTabTitle", "MyStandaloneWindowPlugin"))
			.SetMenuType(ETabSpawnerMenuType::Hidden);
	{
		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MyTab0Name,
			FOnSpawnTab::CreateLambda([](const FSpawnTabArgs& SpawnTabArgs) {
			FText WidgetText = LOCTEXT("WindowMyTab1Text", "MyButton");
			return SNew(SDockTab)
				.TabRole(ETabRole::NomadTab)
				[
					// Put your tab content here!
					SNew(SButton)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Fill)
					.OnClicked_Lambda(
						[]() {
							UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__));
							return FReply::Handled();
						}
					)
					[
						SNew(STextBlock)
						.Text(WidgetText)
					]
				];
			}))
			.SetDisplayName(LOCTEXT("MyTab0TabTitle", "MyTab0"))
			.SetMenuType(ETabSpawnerMenuType::Enabled);

		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MyTab1Name,
			FOnSpawnTab::CreateLambda([](const FSpawnTabArgs& SpawnTabArgs) {
			TSharedRef<SSlider> MySlider = SNew(SSlider).Value(0.2f);
			return SNew(SDockTab)
				.TabRole(ETabRole::NomadTab)
				[
					// Put your tab content here!
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						MySlider
					]
					+ SVerticalBox::Slot()
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(STextBlock)
						.Text_Lambda(
							[MySlider]() {
								int32 Result = (int32)(MySlider->GetValue() * 100);
								return FText::FromString(FString::FromInt(Result));
							}
						)
					]
				];
			}))
			.SetDisplayName(LOCTEXT("MyTab1TabTitle", "MyTab1"))
			.SetMenuType(ETabSpawnerMenuType::Disabled);

		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(SMyWidgetName,
			FOnSpawnTab::CreateLambda([](const FSpawnTabArgs& SpawnTabArgs) {
				return SNew(SDockTab)
					.TabRole(ETabRole::NomadTab)
					[
						// Put your tab content here!
						SNew(SMyWidget)
					];
				}))
			.SetDisplayName(LOCTEXT("SMyWidgetTitle", "SMyWidget"))
			.SetMenuType(ETabSpawnerMenuType::Hidden);
	}
}

void FMyStandaloneWindowPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FMyStandaloneWindowPluginStyle::Shutdown();

	FMyStandaloneWindowPluginCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(MyStandaloneWindowPluginTabName);
}

TSharedRef<SDockTab> FMyStandaloneWindowPluginModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	FText WidgetText = FText::Format(
		LOCTEXT("WindowWidgetText", "Add code to {0} in {1} to override this window's contents"),
		FText::FromString(TEXT("FMyStandaloneWindowPluginModule::OnSpawnPluginTab")),
		FText::FromString(TEXT("MyStandaloneWindowPlugin.cpp"))
		);

	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			// Put your tab content here!
			SNew(SBox)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(WidgetText)
			]
		];
}

void FMyStandaloneWindowPluginModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->InvokeTab(MyStandaloneWindowPluginTabName);
	{
		FGlobalTabmanager::Get()->InvokeTab(MyTab0Name);
		FGlobalTabmanager::Get()->InvokeTab(MyTab1Name);
		FGlobalTabmanager::Get()->InvokeTab(SMyWidgetName);
	}
}

void FMyStandaloneWindowPluginModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FMyStandaloneWindowPluginCommands::Get().OpenPluginWindow, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FMyStandaloneWindowPluginCommands::Get().OpenPluginWindow));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FMyStandaloneWindowPluginModule, MyStandaloneWindowPlugin)