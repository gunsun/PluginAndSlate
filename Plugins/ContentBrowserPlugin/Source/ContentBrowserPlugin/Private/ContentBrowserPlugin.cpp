// Copyright Epic Games, Inc. All Rights Reserved.

#include "ContentBrowserPlugin.h"
#include "ContentBrowserPluginStyle.h"
#include "ContentBrowserPluginCommands.h"
#include "Misc/MessageDialog.h"
#include "ContentBrowserModule.h"
#include "CollectionManagerTypes.h"
#include "Debugging/SlateDebugging.h"

static const FName ContentBrowserPluginTabName("ContentBrowserPlugin");

#define LOCTEXT_NAMESPACE "FContentBrowserPluginModule"

template<EOperationType T>
static void PrintPaths(const TArray<FName>& Paths) {
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EOperationType"), true);
	if (EnumPtr) {
		for (FName Name : Paths) {
			UE_LOG(LogTemp, Warning, TEXT("%s[%s]"), *EnumPtr->GetNameStringByValue((int64)T), *Name.ToString());
		}
	}
}

void FContentBrowserPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	{
		FContentBrowserPluginStyle::Initialize();
		FContentBrowserPluginStyle::ReloadTextures();

		FContentBrowserPluginCommands::Register();

		PluginCommands = MakeShareable(new FUICommandList);

		PluginCommands->MapAction(
			FContentBrowserPluginCommands::Get().PluginAction,
			FExecuteAction::CreateRaw(this, &FContentBrowserPluginModule::PluginButtonClicked),
			FCanExecuteAction());
	}
	if (FModuleManager::Get().IsModuleLoaded("ContentBrowser")) {
		FContentBrowserModule& ContentBrowser = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");
		
		FAssetViewDragAndDropExtender AssetViewDragAndDropExtender(
			FAssetViewDragAndDropExtender::FOnDropDelegate::CreateLambda(
				[](const FAssetViewDragAndDropExtender::FPayload& Payload) {
					PrintPaths<EOperationType::Drop>(Payload.PackagePaths);
					return false;
				}
			),
			FAssetViewDragAndDropExtender::FOnDragOverDelegate::CreateLambda(
				[](const FAssetViewDragAndDropExtender::FPayload& Payload) {
#define MAX_TIMES (50)
					static int InvokeTimes = MAX_TIMES;
					if (InvokeTimes >= MAX_TIMES) {
						PrintPaths<EOperationType::DragOver>(Payload.PackagePaths);
						InvokeTimes = 0;
					}
					++InvokeTimes;
					return false;
				}
			),
			// 使这个委托可以被调用到： https://github.com/EpicGames/UnrealEngine/pull/7158
			FAssetViewDragAndDropExtender::FOnDragLeaveDelegate::CreateLambda(
				[](const FAssetViewDragAndDropExtender::FPayload& Payload) {
					PrintPaths<EOperationType::DragLeave>(Payload.PackagePaths);
					return false;
				}
			)
		);
		ContentBrowser.GetAssetViewDragAndDropExtenders().Add(AssetViewDragAndDropExtender);

		// debug slate event
#if WITH_SLATE_DEBUGGING
		FSlateDebugging::InputEvent.AddLambda([](const FSlateDebuggingInputEventArgs& Args) {
			const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("ESlateDebuggingInputEvent"), true);
			// 为了让这个打印可以看到： https://github.com/EpicGames/UnrealEngine/pull/7167
			if (Args.InputEventType == ESlateDebuggingInputEvent::DragLeave)
				UE_LOG(LogTemp, Warning, TEXT("%s, %s"), *EnumPtr->GetNameStringByValue((int64)Args.InputEventType), Args.HandlerWidget.IsValid() ? *Args.HandlerWidget->GetTypeAsString() : TEXT("Not Valid"));
			}
		);
#endif

		const auto Extender_SelectedAssets = FContentBrowserMenuExtender_SelectedAssets::CreateLambda(
			[this](const TArray<FAssetData>& Assets) {
				for (FAssetData Asset : Assets) { //打印选中的文件
					UE_LOG(LogTemp, Warning, TEXT("[%s][%s][%s][%s]"),
						*Asset.ObjectPath.ToString(),
						*Asset.PackageName.ToString(),
						*Asset.PackagePath.ToString(),
						*Asset.AssetName.ToString());
				}
				// 构造按钮和子菜单
				TSharedRef<FExtender> Extender = MakeShareable(new FExtender);
				if (FModuleManager::Get().IsModuleLoaded("ContentBrowser")) {
					Extender->AddMenuExtension("CommonAssetActions", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateLambda(
						[](FMenuBuilder& Builder) {
							Builder.BeginSection("ContentBrowserPlugin", LOCTEXT("ContentBrowserPlugin", "ContentBrowserPluginSection"));
							{
								Builder.AddMenuEntry(FContentBrowserPluginCommands::Get().PluginAction);

								Builder.AddSubMenu(
									LOCTEXT("ContentBrowserSubMenu", "SubMenu"),
									LOCTEXT("ContentBrowserSubMenu_ToolTip", "SubMenu ToolTip"),
									FNewMenuDelegate::CreateLambda(
										[](FMenuBuilder& Builder) {
											Builder.BeginSection("ContentBrowserSub", LOCTEXT("ContentBrowserSubHeading", "ContentBrowserSub"));
											{
												Builder.AddMenuEntry(FContentBrowserPluginCommands::Get().PluginAction);
											}
											Builder.EndSection();
										}
									)
								);
							}
							Builder.EndSection();
						}
					));
				}
				return Extender;
			}
		);
		ContentBrowser.GetAllAssetViewContextMenuExtenders().Add(Extender_SelectedAssets);
	}
}

void FContentBrowserPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	FContentBrowserPluginStyle::Shutdown();

	FContentBrowserPluginCommands::Unregister();
}

void FContentBrowserPluginModule::PluginButtonClicked()
{
	// Put your "OnButtonClicked" stuff here
	FText DialogText = FText::Format(
							LOCTEXT("PluginButtonDialogText", "Add code to {0} in {1} to override this button's actions"),
							FText::FromString(TEXT("FContentBrowserPluginModule::PluginButtonClicked()")),
							FText::FromString(TEXT("ContentBrowserPlugin.cpp"))
					   );
	FMessageDialog::Open(EAppMsgType::Ok, DialogText);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FContentBrowserPluginModule, ContentBrowserPlugin)