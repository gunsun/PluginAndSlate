// Copyright Epic Games, Inc. All Rights Reserved.

#include "ContentBrowserPluginCommands.h"

#define LOCTEXT_NAMESPACE "FContentBrowserPluginModule"

void FContentBrowserPluginCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "ContentBrowserPlugin", "Execute ContentBrowserPlugin action", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
