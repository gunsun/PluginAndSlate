// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

UENUM()
enum class EOperationType : uint8 {
	Drop,
	DragOver,
	DragLeave,
};

class FToolBarBuilder;
class FMenuBuilder;

class FContentBrowserPluginModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	/** This function will be bound to Command. */
	void PluginButtonClicked();

private:
	TSharedPtr<class FUICommandList> PluginCommands;

	TSharedPtr<const struct FAssetViewDragAndDropExtender> DragAndDropExtender;
};
