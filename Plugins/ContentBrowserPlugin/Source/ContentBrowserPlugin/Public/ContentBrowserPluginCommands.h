// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "ContentBrowserPluginStyle.h"

class FContentBrowserPluginCommands : public TCommands<FContentBrowserPluginCommands>
{
public:

	FContentBrowserPluginCommands()
		: TCommands<FContentBrowserPluginCommands>(TEXT("ContentBrowserPlugin"), NSLOCTEXT("Contexts", "ContentBrowserPlugin", "ContentBrowserPlugin Plugin"), NAME_None, FContentBrowserPluginStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
