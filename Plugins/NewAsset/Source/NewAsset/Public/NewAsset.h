// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "NewAsset.generated.h"

USTRUCT(BlueprintType)
struct NEWASSET_API FStructMember
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	float FloatValue;
};

UCLASS(BlueprintType)
class NEWASSET_API UClassMember : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	float FloatValue;
};

UCLASS(BlueprintType, Blueprintable)
class NEWASSET_API UNewAsset : public UObject
{
	GENERATED_BODY()
public:
	UNewAsset():ClassMember(Cast<UClassMember>(NewObject<UClassMember>()))
	{}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NewAsset)
	int32 IntValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NewAsset)
	FStructMember StructMember;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NewAsset)
	UClassMember* ClassMember;

	UFUNCTION(BlueprintCallable)
	void Inc() {
		++IntValue;
	}
};
