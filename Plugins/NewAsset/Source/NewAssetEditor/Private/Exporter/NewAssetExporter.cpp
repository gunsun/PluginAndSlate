// Fill out your copyright notice in the Description page of Project Settings.


#include "NewAssetExporter.h"
#include "NewAsset.h"

UNewAssetExporter::UNewAssetExporter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SupportedClass = UNewAsset::StaticClass();
	PreferredFormatIndex = 0;
	FormatExtension.Add(TEXT("myfile"));
	FormatDescription.Add(TEXT("myfile file"));
}

bool UNewAssetExporter::SupportsObject(UObject* Object) const
{
	bool bSupportsObject = false;
	if (Super::SupportsObject(Object))
	{
		bSupportsObject = IsValid(Cast<UNewAsset>(Object));
	}
	return bSupportsObject;
}

bool UNewAssetExporter::ExportBinary(UObject* Object, const TCHAR* Type, FArchive& Ar, FFeedbackContext* Warn, int32 FileIndex, uint32 PortFlags)
{
	UNewAsset* Asset = CastChecked<UNewAsset>(Object);

	if (!IsValid(Asset))
	{
		return false;
	}
	Ar << Asset->IntValue;
	return true;
}
