// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "NewAssetFactoryNew.generated.h"

/**
 * 从4.25.1版开始，若想新资源类型出现在内容浏览器的右键创建菜单中，必须为它注册IAssetTypeActions对象，具体见以下修改
 * https://github.com/EpicGames/UnrealEngine/commit/e8a2922b50b7659edabb9b0779ed9bfc7e593009
 */
UCLASS()
class UNewAssetFactoryNew : public UFactory
{
	GENERATED_UCLASS_BODY()
public:
	virtual UObject* FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;
	virtual bool ShouldShowInNewMenu() const override;
};
