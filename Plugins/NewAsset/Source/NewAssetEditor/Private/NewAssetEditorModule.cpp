// Copyright Epic Games, Inc. All Rights Reserved.

#include "Modules/ModuleManager.h"
#include "AssetToolsModule.h"
#include "Actions/NewAssetAction.h"
#include "DetailCustomization/NewAssetDetailCustomization.h"
#include "PropertyEditorModule.h"

class FNewAssetEditorModule : public IModuleInterface{

public:
	TSharedPtr<IAssetTypeActions> Action;

	virtual void StartupModule() override
	{
		{
			IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
			Action = MakeShareable(new FNewAssetAction());
			AssetTools.RegisterAssetTypeActions(Action.ToSharedRef());
		}

		FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
		PropertyEditorModule.RegisterCustomPropertyTypeLayout("StructMember", FOnGetPropertyTypeCustomizationInstance::CreateLambda(
			[] {return MakeShareable(new FNewAssetMemberCustomization); }
		));
		PropertyEditorModule.RegisterCustomPropertyTypeLayout("ClassMember", FOnGetPropertyTypeCustomizationInstance::CreateLambda(
			[] {return MakeShareable(new FNewAssetMemberCustomization); }
		));
		PropertyEditorModule.RegisterCustomClassLayout("NewAsset", FOnGetDetailCustomizationInstance::CreateLambda(
			[] {return MakeShareable(new FNewAssetDetailCustomization); }
		));
		PropertyEditorModule.NotifyCustomizationModuleChanged();
	}

	virtual void ShutdownModule() override
	{
		{
			FAssetToolsModule* Module = FModuleManager::GetModulePtr<FAssetToolsModule>("AssetTools");
			if (Module) {
				IAssetTools& AssetTools = Module->Get();
				if (Action.IsValid()) {
					AssetTools.UnregisterAssetTypeActions(Action.ToSharedRef());
				}
			}
		}

		FPropertyEditorModule* PropertyEditorModule = FModuleManager::GetModulePtr<FPropertyEditorModule>("PropertyEditor");
		if (PropertyEditorModule) {
			PropertyEditorModule->UnregisterCustomPropertyTypeLayout("StructMember");
			PropertyEditorModule->UnregisterCustomPropertyTypeLayout("ClassMember");
			PropertyEditorModule->UnregisterCustomClassLayout("NewAsset");
			PropertyEditorModule->NotifyCustomizationModuleChanged();
		}
	}
};
	
IMPLEMENT_MODULE(FNewAssetEditorModule, NewAssetEditor)