#include "Actions/NewAssetAction.h"
#include "NewAsset.h"
#include "Toolkits/NewAssetToolkit.h"

uint32 FNewAssetAction::GetCategories()
{
    return EAssetTypeCategories::Basic;
}

FText FNewAssetAction::GetName() const
{
    return FText::FromString(GetSupportedClass()->GetName());
}

UClass* FNewAssetAction::GetSupportedClass() const
{
    return UNewAsset::StaticClass();
}

FColor FNewAssetAction::GetTypeColor() const
{
    return FColor::Red;
}

void FNewAssetAction::OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<IToolkitHost> EditWithinLevelEditor)
{
    EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

	for (auto Obj = InObjects.CreateConstIterator(); Obj; ++Obj)
	{
		auto NewAsset = Cast<UNewAsset>(*Obj);

		if (NewAsset)
		{
			TSharedRef<FNewAssetToolkit> EditorToolkit = MakeShareable(new FNewAssetToolkit());
			EditorToolkit->Initialize(NewAsset, Mode, EditWithinLevelEditor);
		}
	}
}

bool FNewAssetAction::HasActions(const TArray<UObject*>& InObjects) const
{
	return true;
}

void FNewAssetAction::GetActions(const TArray<UObject*>& InObjects, FMenuBuilder& MenuBuilder)
{
	FAssetTypeActions_Base::GetActions(InObjects, MenuBuilder);
	auto Assets = GetTypedWeakObjectPtrs<UNewAsset>(InObjects);
	MenuBuilder.AddMenuEntry(
		FText::FromString(TEXT("NewAssetButton")),
		FText::FromString(TEXT("NewAssetButton_ToolTip")),
		FSlateIcon(),
		FUIAction(
			FExecuteAction::CreateLambda(
				[Assets, InObjects] {
					for (auto Asset : Assets) {
						if (Asset.IsValid()) {
							Asset->Inc();
							Asset->MarkPackageDirty();
						}
					}
				}
			),
			FCanExecuteAction::CreateLambda(
				[Assets] {
					for (auto Asset : Assets) {
						if (Asset.IsValid()) {
							return true;
						}
					}
					return false;
				}
			)
		)
	);
}
