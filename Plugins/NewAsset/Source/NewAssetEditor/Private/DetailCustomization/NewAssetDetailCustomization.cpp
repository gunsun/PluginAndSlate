#include "DetailCustomization/NewAssetDetailCustomization.h"
#include "DetailLayoutBuilder.h"
#include "DetailCategoryBuilder.h"
#include "DetailWidgetRow.h"
#include "PropertyCustomizationHelpers.h"
#include "NewAsset.h"

void FNewAssetDetailCustomization::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	IDetailCategoryBuilder& DetailCategoryBuilderInstance = DetailBuilder.EditCategory(TEXT("NewAssetCategory"));
	TSharedPtr<IPropertyHandle> NewAssetIntValueProperty = DetailBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(UNewAsset, IntValue));
	DetailBuilder.AddPropertyToCategory(NewAssetIntValueProperty);
	DetailCategoryBuilderInstance
		.AddCustomRow(FText::FromString(TEXT("NewAssetCustomRow")))
		.WholeRowContent()
		[
			SNew(SProperty, NewAssetIntValueProperty)
			.ShouldDisplayName(false)
			.CustomWidget()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Right)
				.Padding(4.0f, 0.0f)
				.FillWidth(1.0f)
				[
					SNew(STextBlock)
					.Text_Lambda(
						[NewAssetIntValueProperty]
						{
							int32 Value;
							NewAssetIntValueProperty->GetValue(Value);
							return FText::FromString(FString::FromInt(Value));
						}
					)
				]
				+ SHorizontalBox::Slot()
				.Padding(0.0f, 0.0f)
				.VAlign(VAlign_Center)
				.FillWidth(1.0f)
				[
					NewAssetIntValueProperty->CreatePropertyNameWidget()
				]
			]
		];
}

void FNewAssetMemberCustomization::CustomizeHeader(TSharedRef<IPropertyHandle> InPropertyHandle, FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& CustomizationUtils)
{
	HeaderRow
	.NameContent()
	[
		SNew(STextBlock)
		.Text(FText::Format(FText::FromString(TEXT("Name is {0}")), InPropertyHandle->GetPropertyDisplayName()))
	]
	.ValueContent()
	[
		SAssignNew(TextBlock_MemberValue, STextBlock)
	];
}

void FNewAssetMemberCustomization::CustomizeChildren(TSharedRef<IPropertyHandle> InPropertyHandle, IDetailChildrenBuilder& ChildBuilder, IPropertyTypeCustomizationUtils& CustomizationUtils)
{
	if (TextBlock_MemberValue.IsValid()) {
		TSharedPtr<IPropertyHandle> PropertyHandle = InPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FStructMember, FloatValue));
		if (PropertyHandle.IsValid()) {
			//处理FStructMember类型
			float Value;
			PropertyHandle->GetValue(Value);
			TextBlock_MemberValue->SetText(FString::SanitizeFloat(Value));
		}
		else {
			//处理UClassMember类型
			UObject* Obj;
			InPropertyHandle->GetValue(Obj);
			UClassMember* Member = Cast<UClassMember>(Obj);
			if (Member) {
				TextBlock_MemberValue->SetText(FString::SanitizeFloat(Member->FloatValue));
			}
			else {
				TextBlock_MemberValue->SetText(FText::FromString(TEXT("nullptr")));
			}
		}
	}
}
