#pragma once

#include "Toolkits/NewAssetToolkit.h"
#include "NewAsset.h"
#include "Widgets/Input/SSlider.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Text/STextBlock.h"
#include "IDetailsView.h"

namespace NewAssetToolkit {
	static const FName AppIdentifier("NewAssetEditorApp");
	static const FName TabId("NewAssetEditorTab");
}

void FNewAssetToolkit::RegisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::RegisterTabSpawners(InTabManager);
	SAssignNew(TextBlock, STextBlock).Text(
		FText::FromString(FString::FromInt(IsValid(NewAsset) ? NewAsset->IntValue : 0))
	);
	InTabManager->RegisterTabSpawner(NewAssetToolkit::TabId,
		FOnSpawnTab::CreateLambda(
			[this](const FSpawnTabArgs& Args) {
				FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
				FDetailsViewArgs DetailsViewArgs;
				DetailsView = PropertyModule.CreateDetailView(DetailsViewArgs);
				DetailsView->SetObject(NewAsset);

				return SNew(SDockTab)
				.TabRole(ETabRole::PanelTab)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
					[
						DetailsView.ToSharedRef()
					]
					+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Center).VAlign(VAlign_Center)
					[
						TextBlock.ToSharedRef()
					]
					+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Fill).VAlign(VAlign_Center)
					[
						SNew(SSlider).Value(IsValid(NewAsset) ? NewAsset->IntValue / 10000.f : 0)
						.OnValueChanged(
							FOnFloatValueChanged::CreateLambda(
								[this](float Value) {
									if (TextBlock.IsValid()) {
										TextBlock->SetText(FText::FromString(FString::FromInt((int32)(Value * 10000))));
									}
									if (IsValid(NewAsset)) {
										NewAsset->IntValue = (int32)(Value * 10000);
									}
								}
							)
						).OnMouseCaptureEnd_Lambda(
							[this]() {
								NewAsset->MarkPackageDirty();
							}
						)
					]
				];
			})
	);
}

void FNewAssetToolkit::UnregisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(InTabManager);

	InTabManager->UnregisterTabSpawner(NewAssetToolkit::TabId);
}

void FNewAssetToolkit::Initialize(UNewAsset* InNewAsset, const EToolkitMode::Type InMode, const TSharedPtr<IToolkitHost>& InToolkitHost)
{
	NewAsset = InNewAsset;
	const TSharedRef<FTabManager::FLayout> Layout = FTabManager::NewLayout("NewAssetEditorLayout")
		->AddArea(
			FTabManager::NewPrimaryArea()
			->SetOrientation(Orient_Horizontal)
			->Split(
				FTabManager::NewSplitter()
					->SetOrientation(Orient_Vertical)
					->Split
					(
						FTabManager::NewStack()
						->AddTab(GetToolbarTabId(), ETabState::OpenedTab)
					)
					->Split
					(
						FTabManager::NewStack()
						->AddTab(NewAssetToolkit::TabId, ETabState::OpenedTab)
					)
			)
		);

	InitAssetEditor(InMode, InToolkitHost, NewAssetToolkit::AppIdentifier, Layout, true, true, InNewAsset);
	RegenerateMenusAndToolbars();
}

FText FNewAssetToolkit::GetBaseToolkitName() const
{
	return FText::FromString(TEXT("NewAssetBaseToolkit"));
}

FName FNewAssetToolkit::GetToolkitFName() const
{
	return FName("NewAssetToolkit");
}

FLinearColor FNewAssetToolkit::GetWorldCentricTabColorScale() const
{
	return FLinearColor(0.5f, 0.2f, 0.3f, 0.4f);
}

FString FNewAssetToolkit::GetWorldCentricTabPrefix() const
{
	return FString(TEXT("NewAssetEditorPrefix"));
}
