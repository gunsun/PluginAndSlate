// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyEditorToolbarButton.h"
#include "MyEditorToolbarButtonStyle.h"
#include "MyEditorToolbarButtonCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"
#include "IAnimationBlueprintEditorModule.h" //如果头文件路径不认，就刷新VS文件
#include "BlueprintEditorModule.h"
#include "WorkflowOrientedApp/WorkflowTabManager.h"
#include "BlueprintEditor.h"

static const FName MyEditorToolbarButtonTabName("MyEditorToolbarButton");

#define LOCTEXT_NAMESPACE "FMyEditorToolbarButtonModule"

void FMyEditorToolbarButtonModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FMyEditorToolbarButtonStyle::Initialize();
	FMyEditorToolbarButtonStyle::ReloadTextures();

	FMyEditorToolbarButtonCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FMyEditorToolbarButtonCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FMyEditorToolbarButtonModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::RegisterMenus));
}

void FMyEditorToolbarButtonModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FMyEditorToolbarButtonStyle::Shutdown();

	FMyEditorToolbarButtonCommands::Unregister();
}

void FMyEditorToolbarButtonModule::PluginButtonClicked()
{
	// Put your "OnButtonClicked" stuff here
	FText DialogText = FText::Format(
							LOCTEXT("PluginButtonDialogText", "Add code to {0} in {1} to override this button's actions"),
							FText::FromString(TEXT("FMyEditorToolbarButtonModule::PluginButtonClicked()")),
							FText::FromString(TEXT("MyEditorToolbarButton.cpp"))
					   );
	FMessageDialog::Open(EAppMsgType::Ok, DialogText);
}

void FMyEditorToolbarButtonModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("MainFrame.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FMyEditorToolbarButtonCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("AssetEditor.BlueprintEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FMyEditorToolbarButtonCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		FBlueprintEditorModule& BlueprintEditorModule = FModuleManager::LoadModuleChecked<FBlueprintEditorModule>("Kismet");
		BlueprintEditorModule.OnRegisterTabsForEditor().AddRaw(this, &FMyEditorToolbarButtonModule::OnBPToolBarRegister);
	}
	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FMyEditorToolbarButtonCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
				
				FToolMenuEntry& Entry1 = Section.AddEntry(
					FToolMenuEntry::InitToolBarButton(FMyEditorToolbarButtonCommands::Get().PluginAction,
					TAttribute<FText>(),
					TAttribute<FText>(),
					TAttribute<FSlateIcon>(),
					NAME_None,
					"LastButton"));
				Entry1.SetCommandList(PluginCommands);
				Entry1.InsertPosition.Position = EToolMenuInsertType::First;
			}
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("File");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FMyEditorToolbarButtonCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
				Entry.InsertPosition.Position = EToolMenuInsertType::First;
			}
		}
	}

	{
		UToolMenus* ToolMenus = UToolMenus::Get();
		UToolMenu* MyMenu = ToolMenus->RegisterMenu("LevelEditor.MainMenu.MySubMenu");

		FToolMenuSection& Section = MyMenu->AddSection("MySection0");
		Section.AddMenuEntryWithCommandList(FMyEditorToolbarButtonCommands::Get().PluginAction, PluginCommands);
		UToolMenu* MenuBar = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu");
		MenuBar->AddSubMenu(
			"MainMenu",
			"MySection",
			"MySubMenu",
			LOCTEXT("MyMenu", "My")
		);
	}

	//Source\Engine\Source\Editor
	IAnimationBlueprintEditorModule& AnimationBlueprintEditorModule = FModuleManager::LoadModuleChecked<IAnimationBlueprintEditorModule>("AnimationBlueprintEditor");
	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuExtension("HelpApplication", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::AddMenuExtension));
		AnimationBlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuBarExtension("Help", EExtensionHook::After, PluginCommands, FMenuBarExtensionDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::AddMenuBarExtension));
		AnimationBlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddToolBarExtension("Settings", EExtensionHook::After, PluginCommands, FToolBarExtensionDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::AddToolBarExtension));
		AnimationBlueprintEditorModule.GetToolBarExtensibilityManager()->AddExtender(MenuExtender);
	}
	
	//http://wlosok.cz/editor-plugins-in-ue4-3-toolbar-button/
	/*
	If you try to do
	FBlueprintEditorModule& BlueprintEditorModule = FModuleManager::LoadModuleChecked<FBlueprintEditorModule>(“Kismet”),
	the code will compile, but the engine will crash when starting up.
	One solution I found was to change LoadingPhase in .uplugin file to PostEngineInit.
	*/
	{
		FBlueprintEditorModule& BlueprintEditorModule = FModuleManager::LoadModuleChecked<FBlueprintEditorModule>(TEXT("Kismet"));
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuExtension("HelpApplication", EExtensionHook::After, PluginCommands, FMenuExtensionDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::AddMenuExtension));
		BlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}
}

void FMyEditorToolbarButtonModule::AddMenuExtension(class FMenuBuilder& Builder)
{
	Builder.BeginSection(TEXT("MyButton"));
	Builder.AddMenuEntry(FMyEditorToolbarButtonCommands::Get().PluginAction);
	Builder.EndSection();
}

void FMyEditorToolbarButtonModule::AddMenuBarExtension(class FMenuBarBuilder& Builder)
{
	Builder.AddMenuEntry(FMyEditorToolbarButtonCommands::Get().PluginAction);
}

void FMyEditorToolbarButtonModule::AddToolBarExtension(class FToolBarBuilder& Builder)
{
	Builder.BeginSection(TEXT("MyButton"));
	Builder.AddToolBarButton(FMyEditorToolbarButtonCommands::Get().PluginAction);
	Builder.EndSection();
}

void FMyEditorToolbarButtonModule::OnBPToolBarRegister(FWorkflowAllowedTabSet& TabSet, FName Name, TSharedPtr<FBlueprintEditor> BP)
{
	TSharedPtr<FExtender> ToolBarExtender = MakeShareable(new FExtender);
	ToolBarExtender->AddToolBarExtension("Settings", EExtensionHook::After, PluginCommands, FToolBarExtensionDelegate::CreateRaw(this, &FMyEditorToolbarButtonModule::AddToolBarExtension));
	BP->AddToolbarExtender(ToolBarExtender);
}
#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FMyEditorToolbarButtonModule, MyEditorToolbarButton)